* * *

### A learning graph matching method based on integer linear programming

Goals :

*   An introduction to Learning Graph Matching.
*   The graph matching problem is defined
*   An integer linear program is presented
*   A solver based on the integer linear program is implemented
*   Integration of a combinatorial solver into a deep learning architecture.
*   Backpropagation through a combinatorial solver is presented.
*   An application to key point image matching is proposed.
*   Code in Python base PyTorch Numpy, Matplotlib and Gurobi is proposed through a Jupyter Notebook.

The tutorial can be found here. [HTML Notebook](http://romain.raveaux.free.fr/document/AdifferentiablegraphmatchingmethodbasedILPV5.html) ![](http://romain.raveaux.free.fr/document/architectureLearnGMMIP.png)

No Graph Matching Solver
------------------------

With Graph Matching Solver

* * *


-----------------------------------
